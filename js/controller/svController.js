function renderDSSV(svArr) {
    var contentHTML = ""
    for (var i = 0; i < svArr.length; i++) {
        var sv = svArr[i]

        var contentTr = `
        <tr>
        <td>${sv.ma}</td>
        <td>${sv.tenSV}</td>
        <td>${sv.email}</td>
        <td>${sv.tinhDTB().toFixed(2)}</td>
        <td>
        <button onclick="suaSV(${sv.ma})" class="btn btn-primary">Sửa</button>
        <button onclick="xoaSinhVien(${sv.ma})" class="btn btn-danger">Xóa</button>
        </td>
        
        </tr>`
        contentHTML = contentHTML + contentTr
    }
    document.getElementById('tbodySinhVien').innerHTML = contentHTML
}

function layThongTinTuForm() {
    var maSV = document.getElementById('txtMaSV').value
    console.log('maSV: ', maSV);
    var tenSV = document.getElementById('txtTenSV').value
    console.log('tenSV: ', tenSV);
    var email = document.getElementById('txtEmail').value
    console.log('email: ', email);
    var matKhau = document.getElementById('txtPass').value
    console.log('matKhau: ', matKhau);
    var diemToan = document.getElementById('txtDiemToan').value * 1
    console.log('diemToan: ', diemToan);
    var diemLy = document.getElementById('txtDiemLy').value * 1
    console.log('diemLy: ', diemLy);
    var diemHoa = document.getElementById('txtDiemHoa').value * 1
    console.log('diemHoa: ', diemHoa);
    //lưu lại
    var sv = new SinhVien(maSV, tenSV, email, matKhau, diemToan, diemLy, diemHoa)
    return sv;

    
}

function showThongTinLenForm(sv) {
    document.getElementById('txtMaSV').value = sv.maSV
    document.getElementById('txtTenSV').value = sv.tenSV
    document.getElementById('txtEmail').value =sv.email
    document.getElementById('txtPass').value = sv.matKhau
    document.getElementById('txtDiemToan').value = sv.diemToan
    document.getElementById('txtDiemLy').value = sv.diemLy
    document.getElementById('txtDiemHoa').value = sv.diemHoa
}