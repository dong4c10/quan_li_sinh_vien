var dssv = []

var dataJson = localStorage.getItem("sssssss")
//convert ngược từ json về array
if(dataJson != null){
    var dataArr = JSON.parse(dataJson);
    for (var i = 0; i < dataArr.length; i++) {
        var item = dataArr[i];
        var sv = new SinhVien(
            item.ma,
            item.ten,
            item.email,
            item.matKhau,
            item.toan,
            item.ly,
            item.hoa
        );
        dssv.push(sv);
    }
    renderDSSV(dssv);
}
function themSinhVien() {
    var sv = layThongTinTuForm();
    var isValid = 
        kiemTraTrung(sv.ma, dssv)  & kiemTraRong("spanTenSV", sv.tenSV) & kiemTraRong("spanEmailSV", sv.email)
    console.log('dataJson: ', dataJson);
    if(isValid){
        var dataJson = JSON.stringify(dssv)
        localStorage.setItem("sssssss", dataJson )
        dssv.push(sv)
        renderDSSV(dssv)
    }
    
   
    
    console.log('dssv: ', dssv);
     

    var contentHTML = ""
    for (var i = 0; i < dssv.length; i++) {
        var sv = dssv[i]

        var contentTr = `
        <tr>
        <td>${sv.ma}</td>
        <td>${sv.tenSV}</td>
        <td>${sv.email}</td>
        <td>${sv.tinhDTB().toFixed(2)}</td>
        <td>
        <button onclick="suaSV(${sv.ma})" class="btn btn-primary">Sửa</button>
        <button onclick="xoaSinhVien(${sv.ma})" class="btn btn-danger">Xóa</button>
        </td>
        
        </tr>`
        contentHTML = contentHTML + contentTr
    }
    document.getElementById('tbodySinhVien').innerHTML = contentHTML
}

function xoaSinhVien(id){
    var viTri = -1
    for (var i = 0; i < dssv.length; i++) {
        var sv = dssv[i]
        if(sv.ma == id){
            viTri = i
        }
    }
    dssv.splice(viTri, 1)
    console.log('viTri: ', viTri);
    renderDSSV(dssv);
    
}
function suaSV(id){
    console.log("ssssssss",id);
    var viTri = dssv.findIndex((item) => {
        return item.ma == id;
    })
    if(viTri != -1){
        //chặn sửa mã sv
        document.getElementById("txtMaSV").disabled = true
        showThongTinLenForm(dssv[viTri])
        resetForm();
    }
}

function capNhatSinhVien(){
    //bỏ chặn users
    document.getElementById("txtMaSV").disabled = false
    var sv  = layThongTinTuForm();
    var viTri = dssv.findIndex(function(item){
        return item.ma == sv.ma
    })
    if(viTri !== -1){
        dssv[viTri] = sv
        renderDSSV(dssv)
    }
}
function resetForm(){
    document.getElementById("formQLSV").reset()
}