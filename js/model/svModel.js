function SinhVien(_ma, _tenSV, _email, _matKhau, _diemToan, _diemLy, _diemHoa){
    this.ma = _ma;
    this.tenSV = _tenSV;
    this.email = _email;
    this.matKhau = _matKhau;
    this.diemToan = _diemToan;
    this.diemLy = _diemLy;
    this.diemHoa = _diemHoa;
    this.tinhDTB = function(){
        return (this.diemToan + this.diemLy + this.diemHoa) / 3;
    };
}